---
title: Week 10
date: 2018-11-04
---

## What did you do this past week?

On Monday we continued the lecture on SQL and the basics of how to do selects from database tables, while on Wednesday we had the continuation of the Ethical Software Engineer lecture by Dr. Elaine Rich and Dr. Alan Cline. Friday was a bit refreshing, which is when Dr. Downing led us through all the symbols used in python regular expression and some in SQL. It was also nice seeing Dr. Downing acknowledge how keeping track of all the symbols can confuse us. I assume we will need to know all of them for the next exam and will be allowed to write it on our notes paper. 

## What's in your way?

It is good to have phase two of the interactive database project off of our shoulders, especially since a stacked schedule has come onto my plate for this upcoming week. I am getting very close to finishing one of my research projects, but I have to get an exam and another two lab reports out of the way for Tuesday first. 

## What will you do next week?

This upcoming week will be the start of another round of exams, but it will be the last until finals season for me. This week is also the start of finalizing some of the projects in my other classes, while for phase three of the interactive database project I will be trying to organize and refactor some of the front-end so that it is more usable. 

## What was your experience of Project #3?

Project #3 (Phase 2 of IDB) was rather difficult for me I have to admit. I learned some React and some API programming, and a lot of SQL (which I preferred over the other two). This phase definitely had the most crunch time, too, and looking forward it seems we don’t have a lot of time between the next two phases. 

## What's your pick-of-the-week or tip-of-the-week?

My tip of the week is to learn a new programming language you don’t know yet (or start learning a new spoken language if you want). With the IDB project, I’ve had to dip into two languages I’ve never used, but it’s definitely expanded my knowledge. Expansion of skills was also a big topic at the Atlassian talk a week ago, where it was mentioned that it has become more preferred over time than focusing on one single expertise. 
