---
title: Week 11
date: 2018-11-11
---

## What did you do this past week?

This past week we went over a lot more SQL queries and dove straight into writing SQL in HackerRank exercises. On Friday we met with our corresponding developer and customer teams to check in on each other’s progress. These meetings are pretty fun, too, because we eventually stumble upon something both teams struggled with, or found amusing and can relate to. 

## What's in your way?

Besides switching to work on the front-end of our IDB project, everything seems to be going smoothly. I have finished my second round of exams and was able to do well enough that I am now free from taking any more exams this semester except for Test #2 for this class! 

There is also a huge obstacle in my way at the moment that has been eating up a lot of my time. Over a week ago someone crashed into my car at a parking garage and I have been dealing with that every day since. Luckily, no one is hurt and they have already accepted liabilities for insurance, but the whole insurance process is grueling and it seems it is going to last another four or five weeks. 

## What will you do next week?

Next week I will be trying to finalize all the projects I have on my hands at the moment in hopes that they will be out of the way by the time Thanksgiving comes around. A project I am working on in the research lab is also going nicely and fairly quick. On Monday and Tuesday I will be trying to polish the views and pages on our website for the project. After that, the rest of the week should be smooth sailing. 

## What was your experience in learning SQL?

I have really enjoyed learning SQL actually. The combination of thinking through relational algebra in hand with writing the queries is an interesting engagement, although still very similar to other lines of logic. The declarative style of SQL, however, is very nice to experience. It is somewhat of a “break” from constantly thinking about how to program or access something through algorithms. 

## What's your pick-of-the-week or tip-of-the-week?

My tip of the week is to join something within academia that isn’t part of your course schedule. Join a research lab if you want to try to study something through the scientific process, or join one of the startup labs or events if you are keen on practicing your industry skills. Searching [Eureka](https://eureka.utexas.edu/) and subscribing to research opportunity posts through the [UT listserv](https://utlists.utexas.edu/sympa/) is a good place to start if you are looking for research. 
