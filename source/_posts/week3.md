---
title: Week 3
date: 2018-09-16
---

## What did you do this past week? 

This past week I started taking on some programming projects in the research lab I work in in regards to DNA/RNA sequencing. I also read about twenty-five research abstracts to familiarize myself with recent findings in the field and the published datasets. I finished Collatz pretty quickly, too, thanks to the optimizations we reviewed in the class lectures and quizzes. On the topic of the career fair, I reviewed the sort of back-to-basics lectures that we had on Wednesday and Friday. These were great in keeping in mind what nuances change when switching between Python and Java. 

## What's in your way? 

Not much, really, in relevance to software engineering. The A/C unit in my apartment finally got fixed though -- only took a little over two weeks! Besides the coils failing altogether and needing a replacement, apparently there was also a leak because the neighbors below us had shot a nail through a piece of drywall that pierced a part of the pipeline. 

## What will you do next week? 

I’m striving to complete any homework that’s due throughout the entire week by Monday and Tuesday that way I have nothing to worry about during the career fair and interviews. Also a lot of practice and some more practice. I set a path to review everything I need to know for technical interviews, basically starting from the basics and finishing with combinations of algorithms and dynamic programming and I’m almost done. Then, of course, I’ll be at the career fair on Thursday. 
I’ll also be on a project to optimize and automate a lot of the coding protocols that the lab I’m in works with. 

## What's your experience of the in-class exercises? 

I liked how the HackerRank exercise was encouraged to be discussed, and pairing up with neighboring classmates definitely gave a hint of pair programming. Overall a positive experience, but it felt a bit rushed since it was so close to the end of the hour. 

## What's your pick-of-the-week or tip-of-the-week? 

My pick of the week is [f.lux](https://justgetflux.com/). For anyone that programs late at night (or really uses a computer in any capacity) this is a life-saver. It helps against eye strain, and it is useful to put at a low setting even during the day. 